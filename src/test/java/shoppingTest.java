import Pages.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.support.PageFactory;

public class shoppingTest extends TestBase
{
    @Test
    @Order(1)
    public void signIn()
    {
        indexPage homePage = PageFactory.initElements(driver, indexPage.class);
        homePage.openSignInWindow();
        homePage.setUsername("aimldl45944@gmail.com");
        homePage.setPassword("PBne@$9889");
    }

    @Test
    @Order(2)
    public void search()
    {
        logIndexPage found = PageFactory.initElements(driver, logIndexPage.class);
        found.setSearchText("OnePlus 11 5G (Titan Black, 16GB RAM, 256GB Storage)");
        found.submitSearch();
    }

    @Test
    @Order(3)
    public void openProduct()
    {
        foundResultPage foundPage = PageFactory.initElements(driver, foundResultPage.class);
        foundPage.openProductPage();
    }

    @Test
    @Order(4)
    public void addToCart()
    {
        itemPage itemToBePurchased = PageFactory.initElements(driver, itemPage.class);
        itemToBePurchased.shiftNewWindow();
        itemToBePurchased.pushToCart();
        itemToBePurchased.pushToCart();
        itemToBePurchased.OpenCart();
    }

    @Test
    @Order(5)
    public void checkOut()
    {
        CartPage page = PageFactory.initElements(driver, CartPage.class);
        page.checkOut();
    }
}
