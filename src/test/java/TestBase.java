import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestBase
{
    public static WebDriver driver;

    @BeforeAll
    public static void initialize()
    {
        System.setProperty("webdriver.chrome.driver", "/home/amit/Downloads/ecommerceTesting/resources/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://amazon.in");
    }

}
