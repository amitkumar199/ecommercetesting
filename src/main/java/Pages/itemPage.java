package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class itemPage
{
    public itemPage(WebDriver driver)
    {
        this.driver = driver;
    }

    WebDriver driver;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-to-cart-button\"]")
    WebElement pushToTrolly;

    @FindBy(how = How.XPATH, using = "//*[@id=\"attach-sidesheet-view-cart-button\"]/span/input")
    WebElement cartButton;

    public void shiftNewWindow()
    {
        String mainWindow = driver.getWindowHandle();
        Set<String> allWindow = driver.getWindowHandles();

        for(String currentTab : allWindow)
        {
            if(!currentTab.equals(mainWindow)) {
                driver.switchTo().window(currentTab);
            }

        }
    }
    public void pushToCart()
    {
        pushToTrolly.click();
    }

    public void OpenCart()
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
        wait.until(ExpectedConditions.elementToBeClickable(cartButton));
        cartButton.click();
    }
}
