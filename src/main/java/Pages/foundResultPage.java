package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class foundResultPage
{
    WebDriver driver;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "OnePlus 11 5G")
    WebElement product;

    public void openProductPage()
    {
        product.click();
    }
}
